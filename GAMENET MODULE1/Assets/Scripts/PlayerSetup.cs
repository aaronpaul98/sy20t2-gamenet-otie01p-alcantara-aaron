﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Camera camera;

    [SerializeField]
    private TextMeshProUGUI playerName;
    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine)
        {
            transform.GetComponent<MovementController>().enabled = true;
            camera.enabled = true;
        }
        else
        {
            transform.GetComponent<MovementController>().enabled = false;
            camera.enabled = false;
        }


        playerName.text = photonView.Owner.NickName;
    }


}
