﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerNameInputManager : MonoBehaviour
{
    public void SetPlayerUsername(string Username)
    {
        if (string.IsNullOrEmpty(Username))
        {
            Debug.LogWarning("Username is empty");
            return;
        }

        PhotonNetwork.NickName = Username;
    }
}
